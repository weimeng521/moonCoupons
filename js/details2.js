/**
 * Created by FASHVN-001 on 2018/7/9.
 */

//每隔三位小数加逗号
function add_comma_toThousands(num) {
    var num = (num || 0).toString();
    var result = '';
    while (num.length > 3) {
        result = ',' + num.slice(-3) + result;
        num = num.slice(0, num.length - 3);
    }
    if (num) { result = num + result; }
    return result;
}

window.onresize = function ()
{
    //当窗口重绘，重新适应宽度
    autoWidth();
};
var pinming = {
    'A' :'七星',
    'B':'流心',
    'C':'双白'
};
var diquData = {
    'HD':'华东',
    'HN':'华南',
    'HB':'华北',
    'nu':'错误'

};
var pinpaiData = {
    'XGL':'港丽',
    'XXW':'新旺',
    'XYL':'悠乐',
    'HYT':'花悦庭',
    'SBJ':'石板街',
    'MMK':'玛满矿',
    'JJG':'聚酒锅',
    'XFL':'囍凤楼',
    'OFF':'总部'
};
//模拟请求数据
/*var result = [
    {
        'district':'HD',
        'brand':'XGL',
        'product':'C',
        'sqty':'1',
        'pqty':'1'
    },
    {
        'status':true
    }
];*/
// 实现合计








function listValue(result)
{
    var sqtyTotal = 0;
    var pqtyTotal = 0;
    for(var i =0 ;i< result.length-1;i++){
        //console.log(result);
        var diqu = result[i].district; //地区
        var pinpai = result[i].brand; //品牌
        var product = result[i].product;//品名
        var diquResult = diquData[diqu];
        var pinpaiResult = pinpaiData[pinpai];
        var productRusult = pinming[product];
        //console.log(pinpaiResult);
        if(typeof pinpaiResult == 'undefined'){
            pinpaiResult = '错误'
        }
        //计算每列的总数
        sqtyTotal += parseInt(result[i].sqty);
        pqtyTotal += parseInt(result[i].pqty);


        $("<tr> " +
            "<td>"+diquResult+"</td> " +
            "<td>"+pinpaiResult+"</td> " +
           // "<td>"+value.store+"</td> " +
            "<td>"+productRusult+"</td> " +
            "<td class='text-right'>"+add_comma_toThousands(result[i].sqty)+"</td> " +
            "<td class='text-right'>"+add_comma_toThousands(result[i].pqty)+"</td> " +

            "</tr>").appendTo('.tbCon2');


    }


    $("<tr>" +
        "<td>总计:</td>" +
        "<td></td>" +
        "<td></td>" +
        "<td class='text-right'>"+add_comma_toThousands(sqtyTotal)+"</td>" +
        "<td class='text-right'>"+add_comma_toThousands(pqtyTotal)+"</td>" +
        "</tr>").appendTo('.tbCon2');//在table最后面添加一行
}


//固定表头的宽度，自适应user_table的宽度
function autoWidth()
{

    /*
     * for循环
     * */
    var thead1 = $('#fixedhead th');
    var thead2 = $('#user_table th');
    for(var i = 0;i<thead1.length;i++)
    {
        var thead2W = thead2.eq(i).width();
        thead1.eq(i).width(thead2W)
    }



}



/*
* 数据请求ajax
* */
function orderSn()
{

   // var url = "http://mc.fashvn.com/invoker";
    var url = "http://mc.fashvn.com/invoker";
    //var url = "http://138.1.16.150:89/invoker";
    var data = {
        'act' : "detail"
    };
    $.ajax({
        url : url,
        dataType : "json",
        type : "get",
        data : data,
        success : function(result){
           // console.log(result);
            listValue(result);
            autoWidth();
        }
    });

}

orderSn();

