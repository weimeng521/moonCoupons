/**
 * Created by xmsm on 2018/6/19.
 */

/*
 * 获取当前的链接地址
 * */
var $iosDialog2 = $('#iosDialog2'),
    $iosDialog1 = $('#iosDialog1'),
    $iosDialog3 = $('#iosDialog3'),
    $iosDialog0 = $('#iosDialog0');

var url = window.location.href;
//var url = 'http://mc1.fashvn.com/order.html?sn=C3502981865178052321126';
//console.log(url);
var resulturl = url.split('=')[1]; //码的序列号
/*
 * 判断条码是否存在
 * */
if(typeof(resulturl) == 'undefined'){
    // alert('此条码不存在')
    $iosDialog0.fadeIn(200).find('.message').html('此条码不存在');
    $iosDialog1.fadeIn(200).find('.message').html('此条码不存在');
}
$('#dialogs').on('click', '.weui-dialog__btn', function(){
    $(this).parents('.js_dialog').fadeOut(200);
});


//此处是登录界面
/**
 * 获取html的文件名
 * */
getHtmlDocName(url);
//console.log(getHtmlDocName(url));
function getHtmlDocName(str) {

    str = str.substring(str.lastIndexOf("/") + 1);
    str = str.substring(0, str.lastIndexOf("."));
    return str;
}

/*
 * 判断有没有登录
 * */
var user = localStorage.getItem("user");
var pw = localStorage.getItem("pw");
var token = localStorage.getItem("token");
var info = localStorage.getItem("info");
//console.log(infoData)
if(!user && !pw){
    /*window.location.href = "login.html"+'?'+getHtmlDocName(newUrl)+'='+resulturl;*/
    window.location.href = "login1.html"+'?'+'sn'+'='+resulturl;
}
if(!info){
    /*window.location.href = "login.html"+'?'+getHtmlDocName(newUrl)+'='+resulturl;*/
    window.location.href = "login1.html"+'?'+'sn'+'='+resulturl;
}

info =info.toUpperCase();
var infoData = info.substr(0,2);

/*
 * 处理登录页面传来的链接信息
 * */
var nowUrl = window.location.href;
//var snCode = nowUrl.split('?')[1];
//var nowUrl = 'http://mc1.fashvn.com/order.html?sn=C1411365084588837216224';
var snCode = nowUrl.split('=')[1];
//console.log(snCode); //B419327415619723718763

/*
 * 判断核销码/验证码前面的字母
 * 改变图片
 * */
var strNum =snCode.substr(0,1);//substr,第一个参数是开始索引(从0开始)，第二参数是截取的字符串的长度
//console.log(strNum);//'B'
switch (strNum)
{
    case 'A':
        $('.changeImg').attr('src','imgs/a1.png');
        $('.he_btn').css('background-color','#ac8baa');
        break;
    case 'B':
        $('.changeImg').attr('src','imgs/b2.png');
        $('.he_btn').css('background-color','#d49fa9');
        break;
    case 'C':
        $('.changeImg').attr('src','imgs/c3.png');
        $('.he_btn').css('background-color','#97b7d0');
        break;
}


var $loadingToast = $('#loadingToast');
$('#showIOSDialog2').on('click', function(){
    if ($loadingToast.css('display') != 'none') return;
    //checkScanData(snCode); //核对核销码
    checkSnDatas(snCode);

});

/*
 * 处理条码数据
 * */
function checkSnDatas(sn)
{
    // 1、点击核销，先查询该条码的详细信息
    getSnVersion(sn);
    // 2、查询该卡券是否销售
    // 2.1 未销售  显示该券为销售
    // 2.2 已销售  则判断是否核销
    // 2.2.1 已销售 未核销，显示核销
    // 2.2.2 已销售 已核销，表明已经使用。

}
/*
 * 获取卡券最新状态
 * */
function getSnVersion(sn)
{
    $loadingToast.fadeIn(100);
    var url = "http://mc.fashvn.com/invoker";
    //var sn  = "C1411365084588837216224";
    var data = {
        'act' : "v",
        'sn' : sn
    };
    $.ajax({
        url : url,
        dataType : "json",
        type : "get",
        data : data,
        success : function(result){
            console.log(result);
            $loadingToast.fadeOut();
            //var seqL = result.seq;
            //var seqData = seqL.substr(0,2);
            if(result.status == true)
            {
                // 执行DOM操作，将接口查询到的编码数据 dom动态放入页面中。
                $('.ord-min').html(result.voucherCode);
                $('.ser-min').html(result.seq);

                // 如果没有销售的日期，表示该卡券尚未销售，那么执行销售

                /*if(result.sold_date == "null")
                 {
                 //salesSn(sn, token);
                 // 提示内容：该卡券尚未销售，是否立即销售
                 // 弹窗有一个 确认按钮，和取消按钮，当点击确认按钮， 调用 saleSn(sn, token); 取消则不调用
                 /!* if(infoData !== seqData){
                 $("#cross").fadeIn().find('.message').html("不能跨地区销售!!!");
                 return false;
                 }else{
                 $("#iosDialog3").fadeIn().find('.message').html("该卡券尚未销售，是否销售？");

                 }*!/
                 $("#iosDialog3").fadeIn().find('.message').html("该卡券尚未销售，是否销售？");

                 }*/

                /*
                 * 无销售，只有核销
                 * */
                if(result.sold_date == "null" && result.pick_up_date =="null"){
                    // 表明已经销售该卡券，尚未核销
                    $("#orderSnCode").fadeIn().find('.message').html("是否核销该卡券？");

                }
                if(result.sold_date == "null" && result.pick_up_date !="null") {
                    // 表明该卡券已经销售、已经核销
                    $iosDialog2.fadeIn().find('.message').html('该卡券已经核销！'); // 弹出销售成功

                }

            }else{
                $iosDialog2.fadeIn().find('.message').html('该卡券信息不存在！'); // 弹出销售成功

            }
        }
    });

}
// 显示 确认销售弹窗
$('#iosDialog3').find('.sure_win').on('click', function(){
    salesSn(snCode, token,info);
});
$('#iosDialog3').find('.close_win').on('click', function () {
    //点击取消按钮
    $('#iosDialog3').hide();
});

// 显示确认核销弹窗
$('#orderSnCode').find('.sure_win').on('click', function(){
    orderSn(snCode, token,info);
});
$('#orderSnCode').find('.close_win').on('click', function () {
    //点击取消按钮
    $('#orderSnCode').hide();
});
$iosDialog2.find('.sure_btn').on('click', function(){
    $iosDialog2.hide().find('.message').html('');
});

/*
 * 确认销售
 * */
function salesSn(sn, token,info)
{
    $('#iosDialog3').hide();
    $('#loadingToast').fadeIn().find('.message').html('数据加载中');

    var url = "http://mc.fashvn.com/invoker";
    //var sn  = "C1411365084588837216224";
    //var token = "wx2018";
    var data = {
        'act' : "s",
        'token' : token,
        'sn' : sn,
        'info':info
    };
    $.ajax({
        url : url,
        dataType : "json",
        type : "get",
        data : data,
        success : function(result){
            console.log(result);
            $('#loadingToast').hide().find('.message').html('');

            if(result.status == true)
            {

                $iosDialog2.fadeIn().find('.message').html('销售成功');// 弹出销售成功
                // 销售成功，成功后，核销卡券
                //$iosDialog2.fadeIn().find('.message').html('销售成功');// 弹出销售成功
                /*var info = localStorage.getItem("info");
                 var infoData = info.substr(0,2);
                 var infoResult = localStorage.setItem('infoPre',infoData)*/

            }else{
                // 销售失败
                $iosDialog2.fadeIn().find('.message').html('销售失败')
            }

        }
    });

}

/*
 * 核销卡券
 * */
function orderSn(sn, token,info)
{
    $('#orderSnCode').hide();
    $('#loadingToast').fadeIn().find('.message').html('核销中');

    var url = "http://mc.fashvn.com/invoker";
    //var sn  = "C1411365084588837216224";
    //var token = "wx2018";
    var data = {
        'act' : "h",
        'token' : token,
        'sn' : sn,
        'info':info
    };
    $.ajax({
        url : url,
        dataType : "json",
        type : "get",
        data : data,
        success : function(result){
            console.log(result);
            $('#loadingToast').hide().find('.message').html('');
            if(result.status == true)
            {
                // 核销成功，给与提示
                $iosDialog2.fadeIn().find('.message').html('核销成功'); // 弹出销售成功
            }else{
                // 核销失败！！！
                $iosDialog2.fadeIn().find('.message').html('核销失败'); // 弹出销售成功
            }
        }
    });

}




