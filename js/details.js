/**
 * Created by FASHVN-001 on 2018/7/9.
 */

//每隔三位小数加逗号
function add_comma_toThousands(num) {
    var num = (num || 0).toString();
    var result = '';
    while (num.length > 3) {
        result = ',' + num.slice(-3) + result;
        num = num.slice(0, num.length - 3);
    }
    if (num) { result = num + result; }
    return result;
}





//模拟请求数据
/*var result = [
    {
        'district':'HD',
        'brand':'XGL',
        'product':'C',
        'shop':'S001',
        'sqty':'1',
        'pqty':'1'
    },
    {
        'district':'HD',
        'brand':'XGL',
        'product':'C',
        'shop':'S002',
        'sqty':'1',
        'pqty':'1'
    },
    {
        'status':true
    }
];*/

/*
 * 品牌
 * */

/*var areaList = {
    'HD' : {
        'areaname': '华东',
        'brand': {
            'XGL': {
                'name': '港丽',
                'shop': [
                    'S001', 'S002', 'S003', 'S004', 'S005'
                ]
            },
            'XXW': {
                'name': '新旺',
                'shop': [
                    'S001', 'S002', 'S003', 'S004', 'S005'
                ]
            }
        }
    },
    'HB' : {
        'areaname': '华北',
        'brand': {
            'XGL': {
                'name': '港丽',
                'shop': [
                    'S001', 'S002', 'S003', 'S004', 'S005'
                ]
            },
            'XXW': {
                'name': '新旺',
                'shop': [
                    'S001', 'S002', 'S003', 'S004', 'S005'
                ]
            }
        }
    },
    'HN' : {
        'areaname': '华南',
        'brand': {
            'XGL': {
                'name': '港丽',
                'shop': [
                    'S001', 'S002', 'S003', 'S004', 'S005'
                ]
            },
            'XXW': {
                'name': '新旺',
                'shop': [
                    'S001', 'S002', 'S003', 'S004', 'S005'
                ]
            }
        }
    }

};
$.each(areaList, function(i, n){
    if(result[0].district == i)
    {
        console.log(i);
        var areaName = areaList[i].areaname;

        console.log(areaList[i].brand);
        console.log(result[0].brand);
        //if();
    }

});*/

/*
* 店铺
* */
var dianpu = {
    'S001' : '宏伊店',
    'S002' : '正大店',
    'S003' : '港汇店',
    'S004' : '百联店',
    'S005' : '龙之梦店',
    'S006' : '又一城店',
    'S007' : 'K11店',
    'S008' : 'IAPM店',
    'S009' : '大悦城店',
    'S010' : '来福士店',
    'S011' : '大宁店',
    'S012' : '杭大店',
    'S013' : '万象城',
    'S014' : '苏州店',
    'S015' : '南京店',
    'S016' : '长乐店',
    'S017' : '汉口店',
    'S018' : '仙霞店',
    'S019' : '淮海店',
    'S020' : '打浦店',
    'S021' : '八佰伴店',
    'S022' : '虹桥店',
    'S023' : '长寿店',
    'S024' : '莘庄店',
    'S025' : '嘉里城店',
    'S026' : '悦达889店',
    'S027' : '月星店',
    'S028' : '世博园店',
    'S029' : '缤谷店',
    'S030' : '迪士尼',
    'S031' : '南翔店',
    'S032' : '七宝店',
    'S033' : '大华店',
    'S034' : '无锡恒隆',
    'S035' : '南京景枫',
    'S036' : '南京仙林',
    'S037' : '杭州嘉里',
    'S038' : '时代广场',
    'S039' : '长泰店',
    'S040' : '96广场',
    'S041' : '合生汇店',
    'S042' : '长泰店',
    'S043' : '时代广场',
    'S044' : '西郊百联',
    'S045' : '金虹桥',
    'S046' : '万象城店',
    'S047' : '西单大悦',
    'S048' : '富力城店',
    'S049' : 'APM店',
    'S050' : '欧美汇店',
    'S051' : '爱琴海店',
    'S052' : '长盈店',
    'S053' : '温特莱店',
    'S054' : '天河城店',
    'S055' : '高德置地',
    'S056' : '天河城店',
    'S057' : '天河城店',
    'S058' : '天汇igc',
    'S059' : '九方店',
    'S060' : '卓悦汇店',
    'S061' : '皇庭店',
    'S062' : '来福士店',
    'S063' : '皇庭店',
    'S064' : '缤纷城店',
    'S065' : '缤纷城店',
    'S066' : '金融中心',
    'S067' : '久光店 ',
    'SOFF':'-',
    'SFAT':'加工厂',
    '':'错误'
};
var pinming = {
    'A' :'七星',
    'B':'流心',
    'C':'双白'
};
var diquData = {
    'HD':'华东',
    'HN':'华南',
    'HB':'华北',
    'nu':'错误'

};
var pinpaiData = {
    'XGL':'港丽',
    'XXW':'新旺',
    'XYL':'悠乐',
    'HYT':'花悦庭',
    'SBJ':'石板街',
    'MMK':'玛满矿',
    'JJG':'聚酒锅',
    'XFL':'囍凤楼',
    'OFF':'总部'
};

function listValue(result){
    //console.log(result);

   /* var groupOne="";
    var sqty=0;
    var hqty=0;*/
    var sqtyTotal = 0;
    var pqtyTotal = 0;
    for(var i =0 ;i< result.length-1;i++){
        var diqu = result[i].district;
        var pinpai = result[i].brand;
        var shopData = result[i].shop;
        var product = result[i].product;
        var shopRsult = dianpu[shopData];
        var productRusult = pinming[product];
        var diquRusult = diquData[diqu];
        var pinpaiResult = pinpaiData[pinpai];
        //console.log(pinpaiResult);
        if(typeof pinpaiResult == 'undefined'){
            pinpaiResult = '错误'
        }
        //console.log(shopRsult);
        if(typeof shopRsult == 'undefined'){
            shopRsult = '错误'
        }

        //计算每列的总数
        sqtyTotal += parseInt(result[i].sqty);
        pqtyTotal += parseInt(result[i].pqty);

       /* var gone=diqu+pinpai;
        if(gone!=groupOne&&i!=0)	{
            $("<tr> " +
                "<td>"+""+"</td> " +
                "<td>"+""+"</td> " +
                "<td>"+""+"</td> " +
                "<td>"+""+"</td> " +
                "<td class='text-right' style='color:red'>"+add_comma_toThousands(sqty)+"</td> " +
                "<td class='text-right' style='color:red'>"+add_comma_toThousands(hqty)+"</td> " +

                "</tr>").appendTo('.tbCon')
            groupOne=gone;
            sqty=result[i].sqty*1+sqty;
            hqty=result[i].pqty*1+hqty;
        }else{
            sqty=result[i].sqty*1+sqty;
            hqty=result[i].pqty*1+hqty;
            groupOne=gone;
        }*/

        $("<tr> " +
            "<td>"+diquRusult+"</td> " +
            "<td>"+pinpaiResult+"</td> " +
            "<td>"+shopRsult+"</td> " +
            "<td>"+productRusult+"</td> " +
            "<td class='text-right'>"+add_comma_toThousands(result[i].sqty)+"</td> " +
            "<td class='text-right'>"+add_comma_toThousands(result[i].pqty)+"</td> " +
            "</tr>").appendTo('.tbCon')
    }

    //把总计插入最后一行
    console.log(sqtyTotal);
    console.log(pqtyTotal);

    $("<tr>" +
        "<td>总计:</td>" +
        "<td></td>" +
        "<td></td>" +
        "<td></td>" +
        "<td class='text-right'>"+add_comma_toThousands(sqtyTotal)+"</td>" +
        "<td class='text-right'>"+add_comma_toThousands(pqtyTotal)+"</td>" +
        "</tr>").appendTo('.tbCon');//在table最后面添加一行


}
//listValue(result);



/*
* 数据请求ajax
* */
function listData()
{

    //var url = "http://mc.fashvn.com/invoker";
    var url = "http://mc.fashvn.com/invoker";
    var data = {
        'act' : "list"
    };
    $.ajax({
        url : url,
        dataType : "json",
        type : "get",
        data : data,
        success : function(result){
            //console.log(result);
            listValue(result);
            autoWidth(); //自动适应表头
        }
    });


}
listData();

//表头的宽度
function autoWidth()
{

    /*
     * for循环
     * */
    var thead1 = $('#fixedhead th');
    var thead2 = $('#user_table th');
    for(var i = 0;i<thead1.length;i++){
        var thead2W = thead2.eq(i).width();
        thead1.eq(i).width(thead2W);
       // console.log(thead2W);
        //console.log(thead1.eq(i).width(thead2W))

    }



}

/*
window.onresize = function () {
    //当窗口重绘，重新适应宽度
    autoWidth();
};*/
