/**
 * Created by FASHVN-001 on 2018/6/25.
 */
//var url = 'http://mc.fashvn.com/verification.html?sn=B1836067890479971777623';
var url = window.location.href;
//console.log(url);
var resulturl = url.split('=')[1]; //码的序列号
/*
 * 判断条码是否存在
 * */
var $loadingToast = $('#loadingToast'),
    $iosDialog2 = $('#iosDialog2'),
    $iosDialog1 = $('#iosDialog1'),
    $iosDialog0 = $('#iosDialog0');

if(typeof(resulturl) == 'undefined'){
    $iosDialog0.fadeIn(200).find('.message').html('此条码不存在');
    $iosDialog1.fadeIn(200).find('.message').html('此条码不存在');
}
//console.log(resulturl); //B419327415619723718763
//$('.ver-min').html(resulturl);  //输出验证码
$('#dialogs').on('click', '.weui-dialog__btn', function(){
    $(this).parents('.js_dialog').fadeOut(200);
});
/*
 * 判断验证码前面的字母
 * 改变图片
 * */
var strNum =resulturl.substr(0,1);//substr,第一个参数是开始索引(从0开始)，第二参数是截取的字符串的长度
//console.log(strNum);//'B'
switch (strNum)
{
    case 'A':
        $('.changeImg').attr('src','imgs/a1.png');
        $('.he_btn').css('background-color','#ac8baa');
        break;
    case 'B':
        $('.changeImg').attr('src','imgs/b2.png');
        $('.he_btn').css('background-color','#d49fa9');
        break;
    case 'C':
        $('.changeImg').attr('src','imgs/c3.png');
        $('.he_btn').css('background-color','#97b7d0');
        break;
    case 'D':
        $('.changeImg').attr('src','imgs/a.png');
        $('.he_btn').css('background-color','#97b7d0');
        $('.bottom').hide();
        $('ticked-logo').attr('src','imgs/G-logo1.png')
        break;

}

/*
* 点击按钮测试正确与错误
* */
$('#showIOSDialog2').on('click', function(){
    if ($loadingToast.css('display') != 'none') return;
    $loadingToast.fadeIn();
    checkScanData(resulturl); //核对验证码
   /* setTimeout(function () {
        $loadingToast.fadeOut(100);
        $iosDialog2.fadeIn(200); //核销成功界面
    }, 2000);
*/
});

/*
 * 处理条码数据
 * */
function checkScanData(sn)
{
    $.ajax({
        url : "http://mc.fashvn.com/invoker",
        dataType : "json",
        type : "post",
        data : {
            'act' : 'v',  //
            'sn':sn      //验证码的数据
        },
        success : function(result){
            $loadingToast.fadeOut();
            if(result.status == true)
            {
                $iosDialog2.fadeIn(200).find('.message').html('此券为真,验证成功!'); //弹出核销成功
                $('.ver-min').html(result.voucherCode);  //输出验证码
                $('.ser-min').html(result.seq); //显示序列号
            }else if(result.status == false)
            {
                //$loadingToast.fadeOut(100);
                $iosDialog2.fadeIn(200).find('.message').html('没有此券，验证失败！');
            }
        }
    });
}

